<?php
namespace App\Http\Controllers;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use App\Repositories\ProductRepository;

class ProductController extends Controller
{
    private $productRepository;

    public function __construct(ProductRepository $productRepository) {
        $this->productRepository = $productRepository;
    }

    public function getProducts(Request $request)
    {
        $data = $this->productRepository->getProducts($request);
        return response()->json(['code' => Response::HTTP_OK, 'data' => $data], Response::HTTP_OK);
    }

}
