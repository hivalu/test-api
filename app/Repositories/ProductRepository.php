<?php
namespace App\Repositories;
use App\Models\Product;
use App\Models\Product2;
use App\Models\Product3;
use App\Models\Province;
use Illuminate\Support\Facades\DB;

class ProductRepository
{
    public function getProducts($request){
        $query = Product::query();
        if($request->searchText){
            $query->FullTextSearch('name', $request->searchText);
        }
        $query->orderBy('id', 'DESC');
        return  $query->get();
    }
}

