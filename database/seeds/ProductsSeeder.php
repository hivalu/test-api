<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class ProductsSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();
        //DB::table('products')->delete();
        $dataInsert = [];
        for ($i=1; $i <= 1 ; $i++){
            $dataInsert[] = [
                'category_id' => rand(1, 3),
                'price' => rand(10000*10, 30000*10) / 100,
                'name' =>  $faker->name,
                'description' =>  $faker->name,
                'created_at' => date('Y-m-d H:i:s')
            ];
        }
        DB::table('products')->insert($dataInsert);
    }
}
