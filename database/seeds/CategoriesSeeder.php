<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        $dataInsert = [
            [
                'id' => 1,
                'name' => 'category one',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 2,
                'name' => 'category two',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'id' => 3,
                'name' => 'category three',
                'created_at' => date('Y-m-d H:i:s')
            ],
        ];
        DB::table('categories')->insert($dataInsert);
    }
}
