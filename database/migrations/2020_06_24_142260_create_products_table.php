<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('category_id')->index();
            $table->decimal('price', 10, 2)->index();
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->index(['category_id', 'price']);
            $table->timestamps();

        });

        Schema::table('products', function (Blueprint $table) {
            DB::statement('ALTER TABLE products ADD FULLTEXT `name` (`name`)');
            DB::statement('ALTER TABLE products ENGINE = MyISAM');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
